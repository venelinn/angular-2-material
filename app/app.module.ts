import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent }  from './app.component';
import { MdUniqueSelectionDispatcher, OverlayModule } from '@angular2-material/core';
import { MdCardModule } from '@angular2-material/card';
import { MdButtonModule } from '@angular2-material/button';
import { MdInputModule } from '@angular2-material/input';
import { MdRadioModule } from '@angular2-material/radio';
import { MdMenuModule } from '@angular2-material/menu';
import { MdTabsModule } from '@angular2-material/tabs';
import { MdToolbarModule } from '@angular2-material/toolbar';
import { MdTooltipModule } from '@angular2-material/tooltip';
import { MdSidenavModule } from '@angular2-material/sidenav';

import { MdIconModule } from '@angular2-material/icon';
import { MdIconRegistry } from '@angular2-material/icon';

@NgModule({
  imports:      [ 
    BrowserModule, 
    MdCardModule, 
    MdButtonModule, 
    MdIconModule, 
    MdInputModule, 
    MdRadioModule, 
    MdMenuModule, 
    MdTabsModule, 
    MdToolbarModule, 
    MdTooltipModule, 
    MdSidenavModule,
    OverlayModule.forRoot() 
    ],
  declarations: [ AppComponent ],
  bootstrap:    [ AppComponent ],
  providers: [ MdUniqueSelectionDispatcher, MdIconRegistry, MdMenuModule.forRoot().providers ]
})
export class AppModule { }
